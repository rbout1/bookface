import React, { Component } from 'react';
import Appbar from './components/Appbar';
import Container from '@material-ui/core/Container';
import TemporaryDrawer from './components/TemporaryDrawer';
import { ThemeProvider } from '@material-ui/styles';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { createMuiTheme, makeStyles } from '@material-ui/core/styles';
import { blue, red, deepOrange } from "@material-ui/core/colors";
import Home from './pages/Home';
import Landing from './pages/Landing';
import Login from './pages/Login';
import Footer from './components/Footer';
import Profile from './pages/Profile';
import axios from 'axios';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      navDrawer: {
        open: false
      },
      currentUser: {
        id: 0,
        isLoggedIn: false,
        email: ''
      },
      registeredUsers: [],
      posts: [],
      errorMessage: ''
    };

    this.getStateData            = this.getStateData.bind(this);
    this.addPost                 = this.addPost.bind(this);
    this.addComment              = this.addComment.bind(this);
    this.addUser                 = this.addUser.bind(this);
    this.updateCurrentUser       = this.updateCurrentUser.bind(this);
    this.iconLikeClickHandler    = this.iconLikeClickHandler.bind(this);
    // this.iconCommentClickHandler = this.iconCommentClickHandler.bind(this);
    this.updateErrorMessage      = this.updateErrorMessage.bind(this);
  }

  componentDidMount() {
    this.getStateData();
  }

  async getStateData() {
    const registeredUsers = await axios.get('/api/users').then(({data}) => data);
    const posts = await axios.get('/api/posts').then(({data}) => data);

    this.setState({
      posts,
      registeredUsers
    });
  }

  async addPost(post) {
    if (this.state.currentUser.isLoggedIn) {
      if (post.content.length > 0) {
        await axios.post('/api/posts', post)
          .then(({data}) => {
            if (data.content) {
              this.setState({ ...this.state, posts: [...this.state.posts, data] });
            } else {
              this.setState({ ...this.state, errorMessage: data.message });
            }
          })
          .catch(err => console.log(err));
      }
    } else
      alert('No user is currently logged in. Please log in and try again.');
  }

  async addComment(postId, comment) {
    if (comment !== '') {
      const newPosts = [...this.state.posts];
      const newPost = newPosts.find(post => post._id === postId);

      if (newPost !== null) {
        newPosts.forEach(p => p.id === postId ? p.comments.push(comment) : null);
        this.setState({ ...this.state, posts: newPosts });
        newPost.comments.push(comment);
        await axios.put(`/api/posts/${postId}/comment`, newPost)
          .catch(err => console.log(err));
      }
    }
  }

  updateErrorMessage() { this.setState({ ...this.state, errorMessage: '' }); }

  addUser(registeredUser) {
    const newUsers = [...this.state.registeredUsers];
    newUsers.push(registeredUser);
    this.setState({ ...this.state, registeredUsers: newUsers });
  };

  updateCurrentUser(isLoggedIn, email) {
    const user = this.state.registeredUsers.find(u => u.email === email);
    this.setState({
      ...this.state,
      currentUser: {
        id: !!user ? user._id : 0,
        isLoggedIn,
        email
      }
    });
  }

  async iconLikeClickHandler(postId, type) {
    const newPosts = [...this.state.posts];
    const newPost = newPosts.find(post => post._id === postId);
    const { like, dislike } = newPost.icons;

    switch (type) {
      case "like":
        if (like.clickedBy.includes(this.state.currentUser.email))
          like.clickedBy = like.clickedBy.filter(em => em !== this.state.currentUser.email);
        else {
          like.clickedBy.push(this.state.currentUser.email);
          dislike.clickedBy = dislike.clickedBy.filter(em => em !== this.state.currentUser.email);
        }
        break;
      case "dislike":
        if (dislike.clickedBy.includes(this.state.currentUser.email))
          dislike.clickedBy = dislike.clickedBy.filter(em => em !== this.state.currentUser.email);
        else {
          dislike.clickedBy.push(this.state.currentUser.email);
          like.clickedBy = like.clickedBy.filter(em => em !== this.state.currentUser.email);
        }
        break;
      default:
        return;
    }

    await axios.put(`/api/posts/${postId}/like`, newPost)
      .catch(err => console.log(err));

    this.setState({
      ...this.state,
      posts: [
        ...newPosts
      ]
    });
  }

  render() {
    const classes = makeStyles(theme => ({
      root: {
        flexGrow: 1,
      },
      menuButton: {
        marginRight: theme.spacing(2),
      },
      title: {
        flexGrow: 1,
      },
      link: {
        textDecoration: 'none',
        color: 'white',
      },
    }));

    const toggleNavDrawer = () => {
      this.setState({ ...this.state, navDrawer: { ...this.state.navDrawer, open: !this.state.navDrawer.open } });
    };

    const theme = createMuiTheme({
      palette: {
        primary: { main: blue[700] },
        secondary: { main: red[400] },
        error: { main: deepOrange[500] },
      },
    });

    const { currentUser, errorMessage, posts, registeredUsers } = this.state;
    const { isLoggedIn } = this.state.currentUser;

    return (
      <div
        className="App"
        style={{ height: '100vh', display: 'flex', flexDirection: 'column' }}
      >
        <Router>
          <TemporaryDrawer
            toggleDrawer={toggleNavDrawer}
            navDrawerOpen={this.state.navDrawer.open}
            isLoggedIn={isLoggedIn}
            logout={this.updateCurrentUser} />
          <Appbar
            isLoggedIn={isLoggedIn}
            logout={this.updateCurrentUser}
            theme={theme}
            toggleDrawer={toggleNavDrawer} />
          <ThemeProvider theme={theme}>
            <Container fixed className={classes.root}>
              <Route exact={true} path={'/'} render={({ history }) => isLoggedIn ? (
                <Home
                  addComment={this.addComment}
                  addPost={this.addPost}
                  currentUser={currentUser}
                  likeIconClick={this.iconLikeClickHandler}
                  commentIconClick={this.iconCommentClickHandler}
                  posts={posts}
                  errorMessage={errorMessage}
                  theme={theme}
                  updateErrorMessage={this.updateErrorMessage}
                  users={this.state.registeredUsers}
                  updateState = {this.getStateData} />) :
                <Landing
                  addUser={this.addUser}
                  history={history}
                  navDrawerOpen={this.state.navDrawer.open}
                  toggleDrawer={toggleNavDrawer} />} />
              <Route exact={true} path={'/profile'} render={({ history }) => {
                if (isLoggedIn)
                  return <Profile user={currentUser} users={registeredUsers} logout = {this.updateCurrentUser} history = {history}/>
                else
                  history.push('/');
                }} />
              <Route exact={true} path={'/login'} render={({ history }) => <Login authLogin={this.updateCurrentUser} history={history} isLoggedIn={isLoggedIn} users={registeredUsers} />} />
              {/* Temporary Home path for development */}
              {/* <Route exact={true} path={'/home'} component={() => (
                <Home
                  addPost={this.addPost}
                  currentUser={currentUser}
                  likeIconClick={this.iconLikeClickHandler}
                  commentIconClick={this.iconCommentClickHandler}
                  posts={posts}
                  theme={theme}
                  users={registeredUsers} />
              )} /> */}
            </Container>
          </ThemeProvider>
        </Router>
        <Footer theme={theme} />
      </div>
    );
  }
}

export default App;
