import React from 'react';

const Footer = props => {
  const date = new Date();
  const { main } = props.theme.palette.primary;

  return <footer style={{
    marginTop: 'auto',
    backgroundColor: main,
    color: '#fff',
    padding: '1rem 0',
    textAlign: 'center'
  }}>
    &copy; Copyright Bookface {date.getFullYear()}
  </footer>
}

export default Footer;
