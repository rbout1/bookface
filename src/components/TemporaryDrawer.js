import React, { Fragment } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Home from '@material-ui/icons/Home';
import AccountCircle from '@material-ui/icons/AccountCircle';
import ExitToApp from '@material-ui/icons/ExitToApp';
import { Link } from 'react-router-dom';

const useStyles = makeStyles({
  list: {
    width: 250,
  },
  fullList: {
    width: 'auto',
  },
});

const TemporaryDrawer = ({ isLoggedIn, logout, navDrawerOpen, toggleDrawer }) => {
  const classes = useStyles();

  const sideList = side => (
    <div
      className={classes.list}
      role="presentation"
      // onClick={toggleDrawer(side, false)}
      onKeyDown={() => toggleDrawer(side, false)}
    >
      <List>
        <Link
          to={'/'}
          style={{ textDecoration: 'none', color: 'rgba(0, 0, 0, 0.87)' }}>
          <ListItem button onClick={() => toggleDrawer(side, false)}>
            <ListItemIcon><Home /></ListItemIcon>
            <ListItemText primary={'Home'} />
          </ListItem>
        </Link>
        {isLoggedIn ?
          <Fragment>
            <Link
              to={'/profile'}
              style={{ textDecoration: 'none', color: 'rgba(0, 0, 0, 0.87)' }}>
              <ListItem button onClick={() => toggleDrawer(side, false)}>
                <ListItemIcon><AccountCircle /></ListItemIcon>
                <ListItemText primary={'Profile'} />
              </ListItem>
            </Link>
            <ListItem button onClick={() => logout(false, '')}>
              <ListItemIcon><ExitToApp /></ListItemIcon>
              <ListItemText primary={'Logout'} />
            </ListItem>
          </Fragment>

          :
          
          <Link
            to={'/login'}
            style={{ textDecoration: 'none', color: 'rgba(0, 0, 0, 0.87)' }}>
            <ListItem button onClick={() => toggleDrawer(side, false)}>
              <ListItemIcon><ExitToApp /></ListItemIcon>
              <ListItemText primary={'Login'} />
            </ListItem>
          </Link>}
      </List>
    </div>
  );

  return (
    <Drawer open={navDrawerOpen} onClose={() => toggleDrawer('left', false)}>
      {sideList('left')}
    </Drawer>
  );
}

export default TemporaryDrawer;
