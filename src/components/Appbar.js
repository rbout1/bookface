import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Container from '@material-ui/core/Container';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import { Link } from 'react-router-dom';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
    link: {
        textDecoration: 'none',
        color: 'white',
    },
}));

const Navbar = props => {
    const classes = useStyles();
    const { isLoggedIn, logout, mainColor, toggleDrawer } = props;
    const mediaQuery = useMediaQuery('(max-width:599px)');

    const [anchorEl, setAnchorEl] = React.useState(null);

    const handleClick = event => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const handleLogout = () => {
        logout(false, '');
        setAnchorEl(null);
    };

    return (
        <AppBar position="static" style={{ backgroundColor: mainColor, marginBottom: '1rem' }}>
            <Container fixed>
                <Toolbar disableGutters={true}>
                    <IconButton
                        style={{ display: mediaQuery ? 'flex' : 'none' }}
                        edge="start"
                        onClick={() => toggleDrawer()}
                        className={classes.menuButton}
                        color="inherit"
                        aria-label="menu"
                    >
                        <MenuIcon />
                    </IconButton>
                    <Link
                        to={'/'}
                        className={classes.link}
                        style={{
                            marginRight: mediaQuery ? null : 'auto',
                            marginLeft: mediaQuery ? 'auto' : null,
                        }}>
                        <Typography variant="h6" className={classes.title}>
                            Bookface
                        </Typography>
                    </Link>

                    {isLoggedIn ?

                        <div style={{ display: mediaQuery ? 'none' : 'block' }}>
                            <IconButton color={"inherit"} onClick={handleClick}>
                                <AccountCircleIcon fontSize={'large'} />
                            </IconButton>
                            <Menu
                                id="simple-menu"
                                anchorEl={anchorEl}
                                keepMounted
                                open={Boolean(anchorEl)}
                                onClose={handleClose}
                                color={'inherit'}>
                                <Link
                                    to={'./profile'}
                                    style={{textDecoration: 'none', color: 'black'}}
                                >
                                    <MenuItem onClick={handleClose}>Profile</MenuItem>
                                </Link>
                                <Link
                                    to={'./'}
                                    style={{textDecoration: 'none', color: 'black'}}
                                >
                                    <MenuItem onClick={handleLogout}>Logout</MenuItem>
                                </Link>
                            </Menu>
                        </div>

                        :

                        <Link
                            to={'/login'}
                            className={classes.link}
                            style={{ display: mediaQuery ? 'none' : 'inline-flex' }}
                            onClick={() => isLoggedIn ? logout(false, '') : null}
                        >
                            <Button color="inherit" variant={"outlined"}>Log In</Button>
                        </Link>
                    }
                </Toolbar>
            </Container>
        </AppBar>
    )
};

export default function ButtonAppBar({ isLoggedIn, logout, theme, toggleDrawer }) {
    return (
        <Navbar
            isLoggedIn={isLoggedIn}
            logout={logout}
            mainColor={theme.palette.primary.main}
            toggleDrawer={toggleDrawer} />
    );
}
