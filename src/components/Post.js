import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import { ThumbUp, ThumbDown, Comment, Delete } from '@material-ui/icons';
import Axios from 'axios';

export class Post extends Component {
  state = {
    commentContent: ''
  }

  render() {
    const classes = {
      icon: {
        marginRight: '.25rem',
      },
      photoPostUser: {
        borderRadius: '4px 0 0 4px',
        boxShadow: '0 1px 3px rgba(0,0,0,0.2), 0 1px 1px rgba(0,0,0,0.14), 0px 2px 1px rgba(0,0,0,0.12)',
        minWidth: 50,
        width: 50,
        height: 50
      },
      photoCommentUser: {
        borderRadius: '100%',
        marginRight: '.5rem',
        minWidth: 35,
        width: 35,
        height: 35
      }
    }

    const handleCommentContentUpdate = event => this.setState({ ...this.state, commentContent: event.target.value });
    const clearComment = () => this.setState({ ...this.state, commentContent: ''});

    const handleDeleteClick = async () => {
      try{
        await Axios.delete(`http://localhost:4000/api/posts/${post._id}`).then(() => {
          updateState();
        });
      } catch(error){
        console.log(error.message);
      }
    };

    const { addComment, currentUser, likeIconClick, post, theme, users, updateState } = this.props;
    let user = users.find(u => u._id === post.userId);
    if(user === undefined) {
        user = {name: '', email: '', photoURI: ''};
    }
    const postIsYours = currentUser.id === post.userId;

    return (
      <Grid item xs={12} key={`post-${post._id}`} style={{ display: 'flex', marginBottom: '1rem' }}>
        <img
          src={user.photoURI ? user.photoURI : `./assets/img/default-user-icon.jpg`}
          alt="User Icon"
          style={{ ...classes.photoPostUser }}
        />
        <Paper
          // className={classes.paper}
          style={{
            borderRadius: '0 4px 4px 4px',
            padding: '1rem',
            width: 'calc(100% - 50px)'
          }}
        >
          <div style={{ marginBottom: '1rem' }}>
            <h4>{user.name}</h4>
            <p style={{ color: '#AFB3BE' }}>@{user.username}</p>
          </div>
          <p>{post.content}</p>
          <hr style={{ marginTop: '1rem' }} />
          <div style={{ display: 'flex', alignItems: 'center' }}>
            {postIsYours ? 
              <Delete
                style = {{
                  color: 'red', cursor: 'pointer', marginRight: '1rem'
                }}
                onClick = {handleDeleteClick}/>
            : 
              <div></div>
            }
            <ThumbUp
              style={{
                ...classes.icon,
                color: post.icons.like.clickedBy.includes(currentUser.email) ? theme.palette.primary.main : '#AFB3BE',
                cursor: postIsYours ? 'default' : 'pointer'
              }}
              onClick={postIsYours ? null : () => likeIconClick(post._id, "like")} />
            <p
              style={{
                marginRight: '1rem',
                color: post.icons.like.clickedBy.includes(currentUser.email) ? theme.palette.primary.main : '#968A90'
              }}
            >
              {post.icons.like.clickedBy.length}
            </p>
            <ThumbDown
              style={{
                ...classes.icon,
                color: post.icons.dislike.clickedBy.includes(currentUser.email) ? theme.palette.secondary.main : '#AFB3BE',
                cursor: postIsYours ? 'default' : 'pointer'
              }}
              onClick={postIsYours ? null : () => likeIconClick(post._id, "dislike")} />
            <p
              style={{
                marginRight: '1rem',
                color: post.icons.dislike.clickedBy.includes(currentUser.email) ? theme.palette.secondary.main : '#968A90'
              }}
            >
              {post.icons.dislike.clickedBy.length}
            </p>
            <Comment
              style={{
                ...classes.icon,
                color: post.icons.comment.selected ? theme.palette.primary.main : '#AFB3BE'
              }} />
            <p
              style={{
                color: '#968A90'
              }}
            >
              {post.comments.length}
            </p>
          </div>
          <hr style={{ marginBottom: 0 }} />
          <TextField
            id="outlined-textarea"
            // label="With placeholder"
            placeholder="Write a comment..."
            className={classes.textField}
            margin="normal"
            variant="outlined"
            style={{ width: '100%', marginBottom: 0 }}
            autoComplete="off"
            inputProps={{ style: { padding: '.5rem' } }}
            onChange={e => handleCommentContentUpdate(e)}
            value = {this.state.commentContent}
          />
          {this.state.commentContent ? (
            <div style={{ display: 'flex' }} >
              <Button
                variant="contained"
                color="primary"
                style={{ margin: '1rem 0 0 auto' }}
                onClick={() => {
                  addComment(post._id, {
                    id: post.comments.length + 1,
                    userId: currentUser.id,
                    content: this.state.commentContent
                  });
                  clearComment();
                }}
              >
                Post Comment
            </Button>
            </div>
          ) : null}
          {post.comments.length ? (
            post.comments.map(com => {
              if (com.userId !== 0) {
                const user = users.find(u => u._id === com.userId);
                return <div key={`comment-${post._id}-${com.id}`} style={{ display: 'flex', marginTop: '1rem' }}>
                  <img
                    src={user.photoURI ? user.photoURI : `./assets/img/default-user-icon.jpg`}
                    alt="User Icon"
                    style={{ ...classes.photoCommentUser }}
                  />
                  <div style={{ width: '100%' }}>
                    <div style={{ display: 'flex', marginBottom: '.25rem' }}>
                      <h4 style={{ marginRight: '.25rem' }}>{user.name}</h4>
                      <p style={{ color: '#AFB3BE' }}>@{user.username}</p>
                    </div>
                    <p>{com.content}</p>
                  </div>
                </div>
              } else return null;
            })
          ) : null}
        </Paper>
      </Grid >
    )
  }
}

export default Post;
