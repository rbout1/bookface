import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import CloseIcon from '@material-ui/icons/Close';
import ErrorIcon from '@material-ui/icons/Error';
import IconButton from '@material-ui/core/IconButton';

const useStyles = makeStyles(theme => ({
  error: {
      backgroundColor: theme.palette.error.main,
  },
  icon: {
      fontSize: 20,
      margin: 5,
  },
  message: {
      display: 'flex',
      alignItems: 'center',
  }
}));

export default function CustomSnackbar({ handleClose, message, open, updateError }) {

  const classes = useStyles();

  return (
    <Snackbar
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'left',
      }}
      open={open}
      autoHideDuration={6000}
      onClose={() => {
        handleClose();
        if (updateError)
          setTimeout(() => updateError(), 350);
      }}
    >
      <SnackbarContent
        className={classes.error}
        aria-describedby="client-snackbar"
        message={<span className={classes.message}>
                    <ErrorIcon className={classes.icon} />
                    {message}
                </span>}
        action={[
          <IconButton key="close" aria-label="close" color="inherit" onClick={handleClose}>
            <CloseIcon className={classes.icon} />
          </IconButton>,
        ]}
      />
    </Snackbar>
  )
}
