import React from 'react'
import Button from '@material-ui/core/Button';
import Axios from 'axios';

const Profile = props => {

    const { user, users, logout, history } = props;

    const currentUser = users.filter(u => u.email === user.email)[0];

    const handleDelete = async () =>{
        try{
            await Axios.get(`http://localhost:4000/api/posts/check/${currentUser._id}`).then( async response => {
                const post = response.data;
                if(!post){
                    await Axios.delete(`http://localhost:4000/api/users/${currentUser._id}`, currentUser).then(() => {
                        logout(false, '');
                        history.push('/');
                    });
                }
                else {
                    alert('You must delete all of your posts before deleting your account.');
                }
            })
        }catch(error){

        }
    }

    let email = '';
    let name = '';

    if(currentUser !== undefined) {
        email = currentUser.email;
        name = currentUser.name;
    }
    else {
        name = 'You are not logged in';
    }

    return (
        <div>
            <h1>{name}</h1>
            <p>{email}</p>
            <Button
                onClick = {handleDelete}
                variant = 'contained'
                color = 'primary'>
                    Delete account
            </Button>
        </div>
    );
};

export default Profile;
