import React, { useEffect, useState } from 'react'
import { makeStyles } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
// import Container from '@material-ui/core/Container';
import Card from '@material-ui/core/Card';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import CustomSnackbar from '../components/CustomSnackbar';
import Axios from 'axios';

const useStyles = makeStyles(theme => ({
  container: {
    height: '100%',
    display: 'flex'
  },
  card: {
    display: 'flex',
    height: '100%',
    // width: '50%',
    backgroundColor: '#DFDEDE',
    marginBottom: '1rem',
    // marginRight: '-4.5rem',
    flexDirection: 'column',
    float: 'right'
  },
  textField: {
    //display: 'flex',
    width: '100%',
    marginLeft: theme.spacing(5),
    marginRight: theme.spacing(5)
  },
  button: {
    margin: theme.spacing(2),
    width: '40%'
  },
  img: {
    width: '100%',
    height: '30rem',
    float: 'left',
    borderRadius: '0.3rem'
  },
  bulletImg: {
    width: '2rem',
    height: '2rem',
    marginTop: '2rem',
    marginLeft: '1rem',
    display: "inline"
  },
  h1: {
    textAlign: 'center',
    paddingTop: '1rem',
    fontFamily: "Georgia, sans-serif"
  },
  h2: {
    textAlign: 'left',
    paddingTop: '1.5rem',
    marginLeft: '2rem'
  },
  p: {
    display: 'inline',
    paddingLeft: '2rem'
  },
  error: {
    backgroundColor: theme.palette.error.main,
  },
  icon: {
    fontSize: 20,
    margin: 5,
  },
  message: {
    display: 'flex',
    alignItems: 'center',
  }
}));

const Landing = props => {

  const { history, navDrawerOpen, toggleDrawer } = props;

  useEffect(() => {
    navDrawerOpen && toggleDrawer();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const mediaQuery = useMediaQuery('(max-width:1279px)');

  const [registerClick, setRegisterClick] = React.useState(false);

  const [open, setOpen] = React.useState(false);
  
  const classes = useStyles();

  const [userFields, setUserFields] = useState({
    email: '',
    username: '',
    name: '',
    password: '',
    confirmPassword: ''
  });

  const handleUserEmailChange = (event) => {
    setUserFields({ ...userFields, email: event.target.value });
  };

  const handleUsernameChange = (event) => {
    setUserFields({...userFields, username: event.target.value});
  };

  const handleNameChange = (event) => {
    setUserFields({...userFields, name: event.target.value});
  };

  const handleUserPasswordChange = (event) => {
    setUserFields({ ...userFields, password: event.target.value });
  };

  const handleConfirmPasswordChange = (event) => {
    setUserFields({...userFields, confirmPassword: event.target.value});
  };

  const checkIfEmpty = () => {
    return !(userFields.name === "" || userFields.email === "" || userFields.username === "");
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleRegister = async () => {
    if (userFields.password === userFields.confirmPassword && checkIfEmpty()) {
      const user = {
        email: userFields.email,
        username: userFields.username,
        name: userFields.name,
        password: userFields.password,
      };
      try {
        await Axios.post('http://localhost:4000/api/users', user).then(response => {
          const user = response.data;
          if(!user.message){
            history.push('./login');
          } else {
            alert('A user with this email already exists. Please choose a different one.');
          }
        });
      }
      catch(error) {

      }
    }
    else if(userFields.password !== userFields.confirmPassword)
      setOpen(true);
    else
      setRegisterClick(true);
  };

  return (
    <div style={{
      display: 'flex',
      justifyContent: 'center'
    }}>
      <div
        className={classes.container}
        style={{
          display: mediaQuery ? 'none' : 'flex',
          width: '62.5%',
          float: 'left',
          marginRight: '1rem'
        }}>
        <Card>
          <img className={classes.img} src="./assets/img/landing-pic.jpg" alt={"A family"} />
        </Card>
      </div>

      <Card className={classes.card} style={{ width: mediaQuery ? '100%' : '50%' }}>
        <h1 className={classes.h1}>Facebook without the data theft</h1>
        <div>
          <img className={classes.bulletImg} src="./assets/img/post-icon.png" alt={"Bullet"}/>
          <p className={classes.p}><b>Write</b> about your day!</p>
        </div>
        <div>
          <img className={classes.bulletImg} src="./assets/img/share-icon.jpg" alt={"Share"}/>
          <p className={classes.p}><b>Share</b> all of your favorite pictures!</p>
        </div>
        <div>
          <img className={classes.bulletImg} src="./assets/img/connect-icon.jpg" alt={"Connect"}/>
          <p className = {classes.p}><b>Connect</b> with friends all around the world!</p>
        </div>
        <br />
        <h2 className={classes.h2}>Sign up today!</h2>
        <p style={{marginLeft: '2rem', paddingTop: '.5rem'}}>It's free!</p>
        <div style={{display: 'flex', justifyContent: 'center', flexWrap: 'wrap', flexDirection: 'row'}}>
          <TextField
            id="email-input"
            label="Email"
            className={classes.textField}
            type="email"
            name="email"
            autoComplete="email"
            margin="normal"
            variant="filled"
            onChange={handleUserEmailChange}
            required
            error={userFields.email === "" && registerClick}
          />
          <TextField
            id="name-input"
            label="Name"
            className={classes.textField}
            margin="normal"
            variant="filled"
            onChange={handleNameChange}
            required
            error={userFields.name === "" && registerClick}
          />
          <TextField
            id="username-input"
            label="Username"
            className={classes.textField}
            margin="normal"
            variant="filled"
            onChange={handleUsernameChange}
            required
            error={userFields.username === "" && registerClick}
          />
          <TextField
            id="password-input"
            label="Password"
            className={classes.textField}
            type="password"
            autoComplete="current-password"
            margin="normal"
            variant="filled"
            onChange={handleUserPasswordChange}
            required
            error={userFields.password === "" && registerClick}
          />
          <TextField
            id="password-input"
            label="Confirm Password"
            className={classes.textField}
            autoComplete="current-password"
            type="password"
            margin="normal"
            variant="filled"
            onChange={handleConfirmPasswordChange}
            required
            error={userFields.confirmPassword === "" && registerClick}
          />
          <br />
          <Button color= "primary" variant={"contained"} className={classes.button} onClick={handleRegister}>
            Sign Up
          </Button>
        </div>
      </Card>

      <CustomSnackbar handleClose={handleClose} message={'Passwords do not match.'} open={open} />
    </div>
  );
}

export default function Register({addUser, history, navDrawerOpen, toggleDrawer}) {
  return (
      <Landing
          addUser={addUser}
          history={history}
          navDrawerOpen={navDrawerOpen}
          toggleDrawer={toggleDrawer}
      />
  );
}
