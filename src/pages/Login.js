import React, { Fragment, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Card from '@material-ui/core/Card';
import Button from '@material-ui/core/Button';
import CustomSnackbar from '../components/CustomSnackbar';
import axios from 'axios';

const useStyles = makeStyles(theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
    },
    dense: {
        marginTop: theme.spacing(2),
    },
    menu: {
        width: 200,
    },
    card: {
        width: 250,
    },
    button: {
        margin: theme.spacing(1),
    },
    input: {
        display: 'none',
    }
}));

export default function Login({ authLogin, history, users }) {

    const classes = useStyles();

    const [open, setOpen] = React.useState(false);

    const [userFields, setUserFields] = useState({
        email: '',
        password: ''
    });

    const handleUserEmailChange = (event) => {
        setUserFields({ ...userFields, email: event.target.value });
    }
    const handleUserPasswordChange = (event) => {
        setUserFields({ ...userFields, password: event.target.value });
    }

    const handleClose = () => {
        setOpen(false);
    };

    const handleLoginClick = async history => {
        const { email, password } = userFields;
        const user = { email, password };

        if(email === '' || password === '')
            setOpen(true);
        else {
            await axios.post('/api/users/isValid', user)
                .then(async ({ data }) => {
                    if (data !== '' && data.isValid) {
                        authLogin(true, email);

                        // Create a Session
                        await axios.post('/api/sessions/auth/login', { email, password })
                            .then(() => history.push('/'))
                            .catch(err => console.log(err));
                    } else
                        setOpen(true);
                });
        }
    };

    return (
        <Fragment>
            <Card className={classes.card}>
                <TextField
                    id="outlined-email-input"
                    label="Email"
                    className={classes.textField}
                    type="email"
                    name="email"
                    autoComplete="email"
                    margin="normal"
                    variant="outlined"
                    onChange={handleUserEmailChange}
                />
                <br />
                <TextField
                    id="outlined-password-input"
                    label="Password"
                    className={classes.textField}
                    type="password"
                    autoComplete="current-password"
                    margin="normal"
                    variant="outlined"
                    onChange={handleUserPasswordChange}
                />
                <br />
                <Button onClick={() => handleLoginClick(history)} variant={"outlined"} className={classes.button} color={'primary'}>
                    Log In
                </Button>
            </Card>
            <CustomSnackbar handleClose={handleClose} message={'Wrong username or password.'} open={open} />
        </Fragment>
    );
}
