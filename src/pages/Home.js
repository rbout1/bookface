import React, { Component, Fragment } from 'react';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Post from '../components/Post';
import CustomSnackbar from '../components/CustomSnackbar';

export class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      postContent: '',
      snackbar: {
        open: false
      }
    }

    this.clearPost =               this.clearPost.bind(this);
    this.handlePostContentUpdate = this.handlePostContentUpdate.bind(this);
    this.handleSnackbarClose =     this.handleSnackbarClose.bind(this);
  };

  componentDidUpdate(prevProps, prevState){
    if (prevProps.errorMessage !== this.props.errorMessage) {
      if (this.props.errorMessage.length > 0)
        this.handleSnackbarClose();
    }
  }

  clearPost() { this.setState({ ...this.state, postContent: '' }); }
  handlePostContentUpdate(event) { this.setState({ ...this.state, postContent: event.target.value }); }
  handleSnackbarClose() {
    this.setState({ ...this.state, snackbar: { ...this.state.snackbar, open: !this.state.snackbar.open } });
  }

  render() {
    const { snackbar } = this.state;

    const {
      addComment,
      addPost,
      currentUser,
      likeIconClick,
      // commentIconClick,
      errorMessage,
      posts,
      theme,
      updateErrorMessage,
      users,
      updateState
    } = this.props;

    return (
      <Fragment>
        <Grid container spacing={0}>
          <Grid item xs={12}>
            <Paper
              // className={classes.paper}
              style={{ display: 'flex', flexDirection: 'column', marginBottom: '1rem', padding: '.5rem 1rem 1rem 1rem' }}
            >
              <TextField
                id="outlined-textarea"
                label="Write Your Status"
                placeholder="What's on your mind?"
                autoComplete="off"
                multiline
                fullWidth
                // className={classes.textField}
                margin="normal"
                variant="outlined"
                onChange={this.handlePostContentUpdate}
                value = {this.state.postContent}
              />
              <Button
                // className={classes.button}
                style={{ margin: '.5rem 0 0 auto' }}
                variant="contained"
                color="primary"
                onClick={() => {
                  // const tempUser = users.find(u => u.email === currentUser.email);
                  if (currentUser.isLoggedIn) {
                    addPost({
                      // id: posts.length ? posts[posts.length - 1].id + 1 : 1,
                      userId: currentUser.id,
                      content: this.state.postContent,
                      icons: {
                        like: {
                          clickedBy: []
                        },
                        dislike: {
                          clickedBy: []
                        },
                        comment: {
                          selected: false
                        }
                      },
                      comments: []
                    });
                    this.clearPost();
                  } else
                    alert('No user is currently logged in. Please log in and try again.');
                }}
              >
                Post
              </Button>
            </Paper>
          </Grid>
          {posts.slice(0).reverse().map(post => (
            <Post
              key={post._id}
              addComment={addComment}
              currentUser={currentUser}
              likeIconClick={likeIconClick}
              post={post}
              theme={theme}
              users={users}
              updateState={updateState} />
          ))}
        </Grid>
        <CustomSnackbar
          handleClose={this.handleSnackbarClose}
          message={errorMessage}
          open={snackbar.open}
          updateError={updateErrorMessage} />
      </Fragment>
    )
  }
}

export default Home;
